Datenschutzvorlagen nach DSGVO / BDSG-neu
=========================================

Hier entsteht eine Sammlung von Vorlagen zum Datenschutz für die Piratenpartei Deutschland.

Alle Angaben ohne Gewähr! :)


Auftragsverarbeitungsvertrag
----------------------------

Die Vorlage basiert auf einer Formulierungshilfe des Landesbeauftragten für Datenschutz und Informationsfreiheit Baden-Württemberg: https://www.baden-wuerttemberg.datenschutz.de/wp-content/uploads/2018/01/muster_adv.pdf 
Die Änderungen zur Vorlage sind über die Historie des Repositories nachvollziehbar.
Variable Teile sind im "Hauptvertrag" in `auftragsverarbeitung_vertrag.md` zu finden.
Dazu gehören die allgemeinen Bedingungen zu Auftragsverarbeitungsverträgen in `auftragsverarbeitung_allgemeine_bestimmungen.md`.

Lizenz
------

[CC0 1.0 Universal](https://choosealicense.com/licenses/cc0-1.0), siehe auch LICENSE

Inhalte aus diesem Repository können frei kopiert und verändert werden.
