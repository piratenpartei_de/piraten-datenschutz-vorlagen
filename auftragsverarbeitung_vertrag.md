# Vertrag zur Verarbeitung im Auftrag nach Art. 28 Abs. 3 DS-GVO

Zwischen der Piratenpartei Deutschland, Pflugstraße 9a, 10115 Berlin, vertreten durch den Vorstand, nachstehend Auftraggeber genannt

und 

`_____________________________`

nachstehend Auftragnehmer genannt

wird ein 

__Vertrag zur Verarbeitung von Daten im Auftrag__

geschlossen.

Weitere Vereinbarungen ergeben sich aus den allgemeinen Datenschutz- und Datensicherheitsbestimmungen (Anlage 1 zu diesem Vertrag), soweit hier im Hauptvertrag keine abweichenden Regelungen vereinbart worden sind.


## 1 Gegenstand und Dauer des Auftrags

Der Auftrag beginnt mit der Unterzeichnung des Vertrages und ist jederzeit mit einer Frist von 30 Tagen zum Monatsende von beiden Seiten kündbar.

Gegenstand der Auftragsverarbeitung ist 

`_____________________________`


## 2 Art der Verarbeitung, Art der personenbezogenen Daten sowie Kategorien betroffener Personen


### Art der Verarbeitung 

_(entsprechend der Definition von Art. 4 Nr. 2 DS-GVO)_


`_____________________________`



### Art der personenbezogenen Daten 

_(entsprechend der Definition von Art. 4 Nr. 1, 13, 14 und 15 DS-GVO)_


`_____________________________`


### Kategorien betroffener Personen 

_(entsprechend der Definition von Art. 4 Nr. 1 DS-GVO)_


`_____________________________`



## 3 Weisungsberechtigte des Auftraggebers, Weisungsempfänger des Auftragnehmers, Kommunikationskanäle

Weisungsberechtigte Personen des Auftraggebers sind:


`_____________________________`

_(Vorname, Name, ggf. Position, Mailadresse)_


Weisungsempfänger beim Auftragnehmer sind:


`_____________________________`

_(Vorname, Name, ggf. Position, Mailadresse)_

Für Weisungen und Mitteilungen im Rahmen des Vertragsverhältnisses zu nutzende Kommunikationskanäle:


`_____________________________`

_(z.B. signierte und verschlüsselte E-Mail S/MIME bzw. PGP, Ticketsystem wie z.B. Redmine, ...)_


## 4 Pflichten des Auftragnehmers, Datenschutzbeauftragte(r)


Beim Auftragnehmer ist als Beauftragte(r) für den Datenschutz bestellt:


`_____________________________`

_(Vorname, Name, Mailadresse)_


## 5 Unterauftragsverhältnisse mit Subunternehmern 

_gemäß (Art. 28 Abs. 3 Satz 2 lit. d DS-GVO)_

Es bestehen zur Zeit keine den Gegenstand dieses Vertrags betreffende Unterauftragsverhältnisse mit Subunternehmern.

_oder_

Zurzeit sind für den Auftragnehmer die folgenden Subunternehmer mit der Verarbeitung von personenbezogenen Daten in dem dort genannten Umfang beschäftigt.
Mit deren Beauftragung erklärt sich der Auftraggeber einverstanden.


`_____________________________`


## 6 Technische und organisatorische Maßnahmen

Das im Anhang 2 beschriebene Datenschutzkonzept stellt die Auswahl der technischen und organisatorischen Maßnahmen passend zum ermittelten Risiko unter Berücksichtigung der Schutzziele nach Stand der Technik detailliert und unter besonderer Berücksichtigung der eingesetzten IT-Systeme und Verarbeitungsprozesse beim Auftragnehmer dar.


## Anlagen

(1) allgemeine Datenschutz- und Datensicherheitsbestimmungen
(2) Beschreibung der TOM lt. Art. 32 DS-GVO 


## Datum und Unterschriften

&nbsp;

`_______________________________________________________________________________________`

_Auftraggeber_

&nbsp;

`_______________________________________________________________________________________`

_Auftraggeber_

&nbsp;

`_______________________________________________________________________________________`

_Auftraggeber_

&nbsp;

`_______________________________________________________________________________________`

_Auftraggeber_

&nbsp;

`_______________________________________________________________________________________`

_Auftraggeber_

&nbsp;

`___________________________________________________________________________`


_Auftragnehmer_
